# @otw/mask-react

Table Of Contents:

- [Install](#install)
- [Usage](#usage)
- [API](#api)
- [CONTRIBUTE](#contribute)
  - [Local Installation](#local-installation)
  - [Run Tests](#run-tests)
  - [Build](#build)

## Install

```bash
npm i --save @otw/mask-react

yarn add @otw/mask-react
```

:warning: Under the hood, it relies on [@otw/mask](https://gitlab.com/thomasjuster/on-the-wire/tree/master/packages/mask)

## Usage

See mask examples in [@otw/mask documentation](https://gitlab.com/thomasjuster/on-the-wire/tree/master/packages/mask). Here I take the credit card example.

```jsx
import React from 'react'
import { InputMask } from '@otw/mask-react'

const mask = '.... .... .... ....'
const validators = { '.': /[0-9]/ }
export function CreditCardInput ({ value, onChange, onBlur, onFocus }) {
  return (
    <InputMask
      mask={mask}
      validators={validators}
      value={value} // raw value here, something like '1111222233334444'
      onChange={(e, { value, maskedValue }) => onChange(e, value)}
      onBlur={(e, { value, maskedValue }) => onBlur(e, { value, maskedValue })}
      onFocus={(e, { value, maskedValue }) => onFocus(e, { value, maskedValue })}
    />
  )
}
```

## API

```tsx
export interface InterfaceProps extends React.HTMLProps<HTMLInputElement> {
  mask: string;
  value: string;
  validators?: OTWMaskValidators; // type from @otw/mask → https://gitlab.com/thomasjuster/on-the-wire/tree/master/packages/mask
  ref?: React.Ref<HTMLInputElement>;
  _ref?: React.Ref<HTMLInputElement>;
  onChange: (e: React.ChangeEvent<HTMLInputElement>, data?: { value: string; maskedValue: string }) => void;
  onBlur?: (e: React.ChangeEvent<HTMLInputElement>, data?: { value: string; maskedValue: string }) => void;
  onFocus?: (e: React.ChangeEvent<HTMLInputElement>, data?: { value: string; maskedValue: string }) => void;
}

export function InputMask (props: InterfaceProps): ReactNode
```

## CONTRIBUTE

This library is provided as is, without any intention to add functionalities or anything. I intend to keep this package _extremely simple_, with simple masking functionalities.

If there are any bugs though, you can still submit a pull request, which I'll check out if relevant.

### Local Installation

```bash
git clone git@gitlab.com:thomasjuster/on-the-wire.git
cd ./on-the-wire
yarn
cd ./packages/mask-react
```

### Run Tests

```bash
yarn test
```

### Build

```bash
# development build
yarn make

# production build
yarn build
```
