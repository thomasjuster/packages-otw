import { configure, addParameters } from '@storybook/react'
import './ui.scss'

function loadStories () {
  require('../src/InputMask.stories')
}

addParameters({
  options: {
    panelPosition: 'bottom',
  },
})

configure(loadStories, module)
