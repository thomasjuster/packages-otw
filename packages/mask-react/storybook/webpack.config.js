const path = require('path')
// const TSDocgenPlugin = require('react-docgen-typescript-webpack-plugin')

module.exports = async ({ config }) => {
  config.module.rules.push({
    test: /\.(ts|tsx)$/,
    include: [path.resolve(__dirname, '../src'), path.resolve(__dirname)],
    use: [require.resolve('ts-loader')],
  }, {
    test: /\.scss$/,
    use: [require.resolve('style-loader'), require.resolve('css-loader'), require.resolve('sass-loader')]
  })
  config.resolve.extensions.push('.ts', '.tsx')
  // config.plugins.push(new TSDocgenPlugin())

  return config
}
