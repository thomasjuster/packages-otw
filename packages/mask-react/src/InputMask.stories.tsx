// tslint:disable:no-eval
// tslint:disable:no-console
import * as React from 'react'
import { storiesOf } from '@storybook/react'
import { InputMask } from './InputMask'

const { Fragment, useState } = React
function mapValues (obj, mapper) {
  return Object.keys(obj).reduce((acc, key) => ({ ...acc, [key]: mapper(acc[key], key) }), obj)
}
function entries (obj) {
  return Object.keys(obj).map((key) => [key, obj[key]])
}

const stories = storiesOf('InputMask', module)

function Demo (props) {
  const [value, setValue] = useState('')
  function onChange (e, data) {
    console.log('[onChange] values:', data)
    setValue(data.value)
  }
  return (
    <div style={{ padding: '1em', boxShadow: '0 0 14px -10px', margin: '2em' }}>
      <h3>{props.title}</h3>
      <code className='block-code'>
        <b>Validators</b>: {'{'}<br />
          {entries(props.validators).map(([char, validator]) => <Fragment>&nbsp;&nbsp;'{char}': /{validator}/<br /></Fragment>)}
        {'}'}
      </code>
      <label className='label'>
        Mask: <code className='code'>'{props.mask}'</code>
      </label>
      <InputMask
        value={value}
        mask={props.mask}
        validators={mapValues(props.validators, (regex) => new RegExp(regex))}
        onBlur={(e, data) => console.log('[onBlur] values:', data)}
        onFocus={(e, data) => console.log('[onFocus] values:', data)}
        onChange={onChange}
      />
    </div>
  )
}

function addDemo (props) {
  stories.add(props.title, () => (
    <Demo title={props.title} mask={props.mask} validators={props.validators} />
  ))
}

addDemo({
  title: 'Alphabetical And Numeric Mask',
  mask: 'a... (nn) ....',
  validators: {
    'a': '[A-z]',
    '.': '.',
    'n': '[0-9]',
  },
})
addDemo({
  title: 'Birthdate',
  mask: 'Dd/Mm/yYYY',
  validators: {
    D: '[0-3]',
    d: '[0-9]',
    M: '[01]',
    m: '[0-9]',
    y: '[12]',
    Y: '[0-9]',
  },
})
addDemo({
  title: 'Basic Credit Card Template',
  mask: '.... .... .... ....',
  validators: { '.': '[0-9]' },
})
addDemo({
  title: 'Guitar Tuning Input',
  mask: '*. *. *. *. *. *. *. *. *. *. *.',
  validators: {
    '*': '[A-G]',
    '.': '[#b]',
  }
})
