import * as React from 'react'
import { applyMask, removeMask, OTWMaskValidators } from '@otw/mask'

export interface InterfaceProps extends React.HTMLProps<HTMLInputElement> {
  mask: string;
  value: string;
  validators?: OTWMaskValidators;
  ref?: React.Ref<HTMLInputElement>;
  _ref?: React.Ref<HTMLInputElement>;
  onChange: (e: React.ChangeEvent<HTMLInputElement>, data?: { value: string; maskedValue: string }) => void;
  onBlur?: (e: React.ChangeEvent<HTMLInputElement>, data?: { value: string; maskedValue: string }) => void;
  onFocus?: (e: React.ChangeEvent<HTMLInputElement>, data?: { value: string; maskedValue: string }) => void;
}

const defaultValidators = {
  '.': /./,
}

export function InputMask (props: InterfaceProps) {
  const {
    mask,
    value,
    validators = defaultValidators,
    _ref,
    onChange,
    onBlur,
    onFocus,
    className,
    ...htmlProps
  } = props

  function forwardEvent (eventName: 'onBlur' | 'onFocus' | 'onChange') {
    if (!props[eventName]) return undefined
    return (e) => {
      const unmaskedValue = removeMask({ value: e.target.value, mask, validators })
      const maskedValue = applyMask({ value: unmaskedValue, mask, validators })
      props[eventName](e, { value: unmaskedValue, maskedValue })
    }
  }

  return (
    <input
      type='text'
      ref={_ref}
      value={applyMask({ value, mask, validators })}
      onChange={forwardEvent('onChange')}
      onBlur={forwardEvent('onBlur')}
      onFocus={forwardEvent('onFocus')}
      {...htmlProps}
    />
  )
}
