/* eslint-disable security/detect-non-literal-regexp,security/detect-object-injection */
'use strict'

const { applyMask, removeMask } = require('@otw/mask')

function forward ({ attributes, except = [] }) {
  return [...attributes]
    .filter((attr) => !except.includes(attr.name))
    .map((attr) => [attr.name, attr.value].join('='))
    .join(' ')
}

function memoize (func) {
  let thunk
  let result
  return (arg, ...args) => {
    if (arg === thunk) return result
    result = func(arg, ...args)
    return result
  }
}

const validatorsFromString = memoize((strValidators) => {
  return strValidators
    .split(',')
    .reduce((acc, validator) => {
      const [maskChar, regex] = validator.trim().split('=')
      return { ...acc, [maskChar]: regex }
    }, {})
})
function validatorsToString (validators = {}) {
  return Object
    .keys(validators)
    .map((maskChar) => `${maskChar}=${validators[maskChar]}`)
    .join(', ')
}

class InputMask extends HTMLElement {
  static get observedAttributes () {
    return ['value', 'mask', 'validators']
  }

  get value () { return this.getAttribute('value') }
  set value (value) { this.setAttribute('value', value) }

  get rawValue () {
    return removeMask({ value: this.value, mask: this.mask, validators: this.validators })
  }

  get mask () { return this.getAttribute('mask') }
  set mask (value) { this.setAttribute('mask', value) }

  get validators () { return this.getAttribute('validators') }
  set validators (value) { this.setAttribute('validators', typeof value === 'string' ? value : validatorsToString(value)) }

  constructor () {
    super()
    this.root = this
    if (this.innerHTML) {
      // eslint-disable-next-line no-console
      console.error('Cannot provide children to <otw-inputmask /> element')
      this.innerHTML = ''
    }
  }

  connectedCallback () {
    this.root.innerHTML =
      `<input type="text" ${forward({ attributes: this.attributes, except: ['type', 'mask', 'validators'] })}>`
    this.input = this.root.querySelector('input')
    this.input.addEventListener('input', (e) => {
      this.updateAttributes()
      this.dispatchEvent(new Event('change'))
    })
    this.mounted = true
    this.updateAttributes(this.value)
  }

  attributeChangedCallback (name, prev, next) {
    if (!this.mounted || prev === next) return
    this.updateAttributes(this.value)
  }

  updateAttributes (nextValue = this.input.value) {
    const validators = validatorsFromString(this.validators)
    const unmaskedValue = removeMask({ value: nextValue, mask: this.mask, validators })
    this.value = applyMask({ value: unmaskedValue, mask: this.mask, validators })
    this.input.value = this.value
    this.input.setAttribute('value', this.value)
  }
}

customElements.define('otw-inputmask', InputMask)
