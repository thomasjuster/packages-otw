# @otw/mask-component

Custom Input Element for @otw/mask

## Usage

```html
<otw-inputmask id='toto' mask="..../nnnn/...." validators=".=[.], n=[0-9]"></otw-inputmask>
<script>
;(() => {
  const input = document.getElementById('toto');
  // NOTE: you can watch the events 'change' and 'input'
  toto.addEventListener('change', () => {
    console.info('value (input.value)', e.target.value);
    console.info('raw value (input.rawValue)', e.target.rawValue);
  });
})();
</script>
```
