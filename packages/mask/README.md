# @otw/mask

Table Of Contents:

- [Install](#install)
- [Usage Examples](#usage-examples)
  - [Birthdate input](#birthdate-input)
  - [Capitalized input for a first/last name](#capitalized-input-for-a-firstlast-name)
  - [Credit Card input and CCV/CVC](#credit-card-input-and-ccvcvc)
  - [Phone number input](#phone-number-input)
- [API](#api)
  - [Validators](#validators)
  - [applyMask (function)](#applymask-function)
  - [removeMask (function)](#removemask-function)
  - [Mask (class)](#mask-class)
- [CONTRIBUTE](#contribute)
  - [Local Installation](#local-installation)
  - [Run Tests](#run-tests)
  - [Build](#build)


## Install

```bash
npm i --save @otw/mask

yarn add @otw/mask
```

## Usage Examples

### Birthdate input

1. The functional programming way

```js
import { applyMask, removeMask } from '@otw/mask'
// or
const { applyMask, removeMask } = require('@otw/mask')

const mask = 'Dd/Mm/yYYY'
const validators = {
  D: /[0-3]/,
  d: /[0-9]/,
  M: /[01]/,
  m: /[0-9]/,
  y: /[12]/,
  Y: /[0-9]/,
}
const maskedValue = applyMask({ mask, validators, value: '10122018' })
// maskedValue → '10/12/2018'
const rawValue = removeMask({ mask, validators, value: '10/12/2018' })
// rawValue → '10122018'
```

2. The object-oriented programming way

```js
import { Mask } from '@otw/mask'
const { Mask } = require('@otw/mask')

const mask = new Mask({
  mask: 'Dd/Mm/yYYY',
  validators: {
    D: /[0-3]/,
    d: /[0-9]/,
    M: /[01]/,
    m: /[0-9]/,
    y: /[12]/,
    Y: /[0-9]/,
  },
})
const maskedValue = mask.apply('10122018')
// maskedValue → '10/12/2018'
const rawValue = mask.remove('10/12/2018')
// rawValue → '10122018'
```

### Capitalized input for a first/last name
```js
import { letterIsBetween } from 'some-utils'

const mask = 'Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'
const validators = {
  A: /[A-Z]/,
  a: /[a-z\-]/,
}
```

### Credit Card input and CCV/CVC
```js
const creditCardMask = '.... .... .... ....'
const ccvMask = '...'
const validators = {
  '.': /[0-9]/,
}
```

### Phone number input
```js
const mask = '... (...)-... ...'
const validators = {
  '.': /[0-9]/,
}
```

## API

### Validators

`validators` is an object in which each key should be present in the mask as the key stores a function used to check whether to accept the input or not at the given position (see examples upper ↑)
```ts
type OTWMaskValidator = RegExp;
type OTWMaskValidators = { [key: string]: OTWMaskValidator; }
```

### applyMask (function)

```ts
function applyMask ({
  value: string;
  mask: string;
  validators?: OTWMaskValidators;
}): string
```

### removeMask (function)

```ts
function removeMask ({
  value: string;
  mask: string;
  validators: OTWMaskValidator;
}): string
```

### Mask (class)

```ts
class Mask {
  private mask: string;
  private validators: OTWMaskValidators;
  constructor ({ mask: string; validators: OTWMaskValidators }): void

  apply (value: string): string
  remove (value: string): string
}
```

## CONTRIBUTE

This library is provided as is, without any intention to add functionalities or anything. I intend to keep this package _extremely simple_, with simple masking functionalities.

If there are any bugs though, you can still submit a pull request, which I'll check out if relevant.

### Local Installation

```bash
git clone git@gitlab.com:thomasjuster/on-the-wire.git
cd ./on-the-wire
yarn
cd ./packages/mask
```

### Run Tests

```bash
yarn test
```

### Build

```bash
# development build
yarn make

# production build
yarn build
```