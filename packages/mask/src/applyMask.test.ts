import { applyMask } from './applyMask'

describe('applyMask', () => {
  const baseValidators = {
    '.': /[ABCabc]/,
    n: /[0-9]/,
  }

  it('should apply regular mask "... ..."', () => {
    const mask = '... ...'
    expect(applyMask({ mask, value: 'ABCabc', validators: baseValidators })).toBe('ABC abc')
  })

  it('should truncate mask "... ..."', () => {
    const mask = '... ...'
    expect(applyMask({ mask, value: 'ABCabcab', validators: baseValidators })).toBe('ABC abc')
  })

  it('should apply mask ".. (..) .."', () => {
    const mask = '.. (..) ..'
    expect(applyMask({ value: 'ABccAB', mask, validators: baseValidators })).toBe('AB (cc) AB')
  })

  it('should apply only until necessary', () => {
    const mask = 'nn/nn/nnnn'
    expect(applyMask({ value: '10', mask, validators: baseValidators })).toBe('10')
  })

  it('should work with optional values', () => {
    const mask = '*. *. *. *. *. *. *. *. *. *. *. *. *. *. *. *. *. *. *.'
    const validators = {
      '*': /[A-G]/,
      '.': /[#b]/,
    }
    expect(applyMask({ value: 'EAbD#GBE', mask, validators })).toBe('E Ab D# G B E')
  })

  it('should apply tuning mask', () => {
    const mask = '*. *. *. *. *. *. *. *. *. *. *. *. *. *. *. *. *. *. *.'
    const validators = {
      '*': /[A-G]/,
      '.': /[#b]/,
    }
    expect(applyMask({ value: 'EAbD#GBH', mask, validators })).toBe('E Ab D# G B')
  })
})