import { Mask } from './Mask'

describe('Mask', () => {
  let mask
  beforeEach(() => {
    mask = new Mask({
      mask: '.. (nn) ..',
      validators: {
        '.': /[A-z]/,
        'n': /[0-9]/,
      },
    })
  })

  it('should instanciate properly', () => {
    expect(mask).toBeTruthy()
  })

  it('should apply mask', () => {
    expect(mask.apply('AB22xxo')).toBe('AB (22) xx')
  })

  it('should remove mask', () => {
    expect(mask.remove('AB (22) xx')).toBe('AB22xx')
  })
})