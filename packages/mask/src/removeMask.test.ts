import { removeMask } from './removeMask'

describe('removeMask()', () => {
  const baseValidators = {
    '.': /[ABCabc]/,
    n: /[0-9]/,
  }
  it('should work with simple mask "... ..."', () => {
    const mask = '... ...'
    const value = 'ABC abc'
    expect(removeMask({ value, mask, validators: baseValidators })).toBe('ABCabc')
  })

  it('should remove complex mask "(..) .. .."', () => {
    const mask = '(..) .. .. nn'
    const value = '(AB) Ca bc 12'
    expect(removeMask({ value, mask, validators: baseValidators })).toBe('ABCabc12')
  })

  it('should truncate when value has more chars than mask', () => {
    const mask = '.. nn ..'
    const value = 'AB bc AB bc'
    expect(removeMask({ value, mask, validators: baseValidators })).toBe('ABbcAB')
  })

  it('should fill progressively', () => {
    const mask = '.. nn ..'
    const value = 'aa 1'
    expect(removeMask({ value, mask, validators: baseValidators })).toBe('aa1')
  })

  it('should fill (very) progressively', () => {
    const mask = '.. nn ..'
    const value = 'a'
    expect(removeMask({ value, mask, validators: baseValidators })).toBe('a')
  })
})