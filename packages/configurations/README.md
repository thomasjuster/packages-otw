# @otw/configurations

Various configurations I re-use constantly for my small-sized projects.
Its main interest is the CLI :
```shell
yarn otwconf help
# outputs
Usage: @otw/conf <command> <OPTIONS>
- commands:
  > app:start
  > app:watch
  > app:build
  > lib:watch
  > lib:build
  > test:ts-react
  > test:ts-node
  > help

- options:
  > --port (app:* only)
  > --entry (lib:* only)
```
