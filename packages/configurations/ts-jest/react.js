'use strict'

// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

const defaults = require('./defaults')

module.exports = ({ modulePaths = [], setupFilesAfterEnv, moduleNameMapper = '' } = {}) => ({
  ...defaults({ modulePaths, setupFilesAfterEnv, moduleNameMapper }),
  preset: 'ts-jest/presets/js-with-babel',
  snapshotSerializers: ['enzyme-to-json/serializer'],
})
