'use strict'

module.exports = ({ modulePaths = [], setupFilesAfterEnv, moduleNameMapper = '' } = {}) => ({
  // An array of directory names to be searched recursively up from the requiring module's location
  moduleDirectories: ['node_modules', '.'],

  // An array of file extensions your modules use
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'node', 'json', 'scss'],

  modulePaths: ['node_modules', '.', 'src', ...modulePaths],

  testRegex: '\\.(test|spec)\\.(t|j)sx?$',

  moduleNameMapper: {
    ...(moduleNameMapper && { '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga|css|scss)$': moduleNameMapper }),
  },
  setupFilesAfterEnv,

  globals: {
    'ts-jest': {
      tsConfig: {
        esModuleInterop: true,
        noImplicitAny: false,
      },
    },
  },
})
