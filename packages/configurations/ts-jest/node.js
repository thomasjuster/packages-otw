'use strict'

// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

const defaults = require('./defaults')

module.exports = ({ modulePaths = [], setupFilesAfterEnv, moduleNameMapper = '' } = {}) => ({
  preset: 'ts-jest',
  ...defaults({ modulePaths, setupFilesAfterEnv, moduleNameMapper }),
  testEnvironment: 'node',
})
