'use strict'

const path = require('path')
const rules = require('../rules')

module.exports = ({
  root,
  entry = path.resolve(root, './src/index.ts'),
  pkgName, // ie: 'shelf', '@otw/shelf'
  output = path.resolve(root, 'lib'),
  NODE_ENV = 'development',
  ...additionalConfig
}) => {
  // const extension = NODE_ENV === 'build' ? 'min.js' : 'js'
  const extension = 'min.js'
  return {
    entry,
    mode: NODE_ENV === 'development' ? 'development' : 'production',
    output: {
      path: output,
      filename: `index.${extension}`,
      library: pkgName.slice(pkgName.lastIndexOf('/') + 1), // avoid scope
      libraryTarget: 'umd',
      umdNamedDefine: true,
    },
    module: {
      rules: [
        rules.tsx({ NODE_ENV, root }),
        rules.css({ NODE_ENV, root }),
        rules.scss({ NODE_ENV, root }),
        rules.images({ NODE_ENV, root }),
        rules.fonts({ NODE_ENV, root }),
      ],
    },
    optimization: {
      minimize: NODE_ENV === 'build',
    },
    resolve: {
      modules: [path.resolve(root, './src'), 'node_modules'],
      extensions: ['.json', '.js', '.jsx', '.ts', '.tsx'],
    },
    ...additionalConfig,
  }
}
