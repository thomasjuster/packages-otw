'use strict'

const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ManifestPlugin = require('webpack-manifest-plugin')

const plugins = {}

module.exports.generics = plugins.generics = ({ root, title = 'React Boilerplate', NODE_ENV, publicPath }) => [
  new HtmlWebpackPlugin({ title, template: path.resolve(__dirname, 'template.ejs') }),
  new ManifestPlugin(),
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(NODE_ENV),
      ASSETS_PATH: JSON.stringify(publicPath),
    },
  }),
]

module.exports.development = plugins.development = (opts = {}) => [
  new webpack.HotModuleReplacementPlugin(),
]
module.exports.preproduction = plugins.preproduction = (opts = {}) => [
  new webpack.HashedModuleIdsPlugin(),
]
module.exports.production = plugins.production = (opts = {}) => [
  new webpack.HashedModuleIdsPlugin(),
]

module.exports = plugins
