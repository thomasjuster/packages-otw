'use strict'

const path = require('path')
const nodeExternals = require('webpack-node-externals')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const rules = require('../rules')

module.exports = ({
  root,
  filename = 'server.js',
  publicPath,
  NODE_ENV,
  entry = './src/server.tsx',
}) => ({
  context: root,
  entry,
  node: {
    __dirname: false,
  },
  output: {
    filename,
    path: path.resolve(root, 'dist'),
    publicPath,
  },
  resolve: {
    cacheWithContext: false, // build time optimization
    extensions: ['.ts', '.tsx', '.js', 'jsx'],
    modules: ['node_modules', path.resolve(root, 'src')],
  },
  target: 'node', // don't bundle standard node.js modules such as http or path
  externals: [nodeExternals({ // don't bundle dependencies from node_modules folder, except by non-javascript files
    whitelist: [ // non-javascript files in node_modules should go to the bundle and be processed by ExtractTextPlugin
      /\.(?!(?:jsx?|json)$).{1,5}$/i,
    ],
  })],
  devtool: NODE_ENV === 'production' ? 'source-map' : 'inline-source-map',
  mode: NODE_ENV === 'production' ? 'production' : 'development',
  module: {
    rules: [
      rules.tsx({ NODE_ENV, root }),
      rules.images({ NODE_ENV, root }),
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader',
        ],
      },
      rules.fonts({ NODE_ENV, root }),
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'styles.css',
      // chunkFilename: '[id]-[hash].css',
    }),
    new CleanWebpackPlugin({ cleanOnceBeforeBuildPatterns: ['./main-*.js'] }),
  ],
  optimization: {
    splitChunks: {
      cacheGroups: {
        styles: {
          name: 'styles',
          test: /\.css$/,
          chunks: 'all',
          enforce: true,
        },
      },
    },
  },
})
