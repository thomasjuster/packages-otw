'use strict'

const path = require('path')
const plugins = require('./plugins')
const rules = require('../rules')

module.exports = ({
  root,
  entry = ['babel-polyfill', './src/index.tsx'],
  NODE_ENV = 'development',
  publicPath = '/static',
  outfilename = '[name]-[contenthash].js',
  devPort,
}, additionalConfig = {}) => ({
  context: root,
  entry,
  output: {
    filename: outfilename,
    path: path.resolve(root, 'dist'),
    publicPath,
  },
  resolve: {
    cacheWithContext: false, // build time optimization
    extensions: ['.ts', '.tsx', '.js', 'jsx'],
    modules: ['node_modules', path.resolve(root, 'src')],
  },
  module: {
    rules: [
      rules.css({ NODE_ENV, root }),
      rules.scss({ NODE_ENV, root }),
      rules.images({ NODE_ENV, root }),
      rules.tsx({ NODE_ENV, root }),
      rules.fonts({ NODE_ENV, root }),
    ],
  },
  plugins: [
    ...plugins.generics({ root, NODE_ENV, publicPath }),
    ...plugins[NODE_ENV](),
  ],

  devServer: {
    compress: true,
    port: devPort,
    contentBase: path.resolve(root, 'dist'),
    historyApiFallback: true,
    disableHostCheck: true,
  },
  // env based config
  devtool: NODE_ENV === 'production' ? 'source-map' : 'inline-source-map',
  mode: NODE_ENV === 'production' ? 'production' : 'development',
  optimization: NODE_ENV === 'development' ? undefined : {
    runtimeChunk: 'single',
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all',
        },
      },
    },
  },
  ...additionalConfig,
})
