declare module "*.css" {
  const content: any;
  export default content;
}
declare module "*.scss" {
  const content: any;
  export default content;
}
declare module "*.sass" {
  const content: any;
  export default content;
}
declare module "*.less" {
  const content: any;
  export default content;
}
declare module "*.png" {
  const content: any;
  export default content;
}
declare module "*.svg" {
  const content: any;
  export default content;
}
declare module "*.jpg" {
  const content: any;
  export default content;
}
declare module "*.gif" {
  const content: any;
  export default content;
}
declare module "*.woff" {
  const content: any;
  export default content;
}
declare module "*.woff2" {
  const content: any;
  export default content;
}
declare module "*.eot" {
  const content: any;
  export default content;
}
declare module "*.ttf" {
  const content: any;
  export default content;
}
declare module "*.otf" {
  const content: any;
  export default content;
}
declare module "*.json" {
  const content: any;
  export default content;
}
declare module "*.js" {
  const content: any;
  export default content;
}
declare module "*.jsx" {
  const content: any;
  export default content;
}