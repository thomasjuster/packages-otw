'use strict'

module.exports.getTypeScriptReactAppConfig = require('./typescript-react')
module.exports.getTypeScriptLibConfig = require('./typescript-lib')
module.exports.rules = require('./rules')
