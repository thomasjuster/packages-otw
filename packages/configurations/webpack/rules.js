'use strict'

const path = require('path')

module.exports.css = ({ NODE_ENV, root } = {}) => ({
  test: /\.css$/,
  use: ['style-loader', 'css-loader'],
})

module.exports.scss = ({ NODE_ENV, root } = {}) => ({
  test: /\.scss$/,
  use: ['style-loader', 'css-loader', 'sass-loader'],
})

module.exports.images = ({ NODE_ENV, root } = {}) => ({
  test: /\.(png|svg|jpe?g|gif)$/,
  use: ['file-loader'],
})

module.exports.tsx = ({ NODE_ENV, root, merge: { use = [], include = [] } = {} } = {}) => ({
  test: /\.tsx?$/,
  use: [
    ...use,
    NODE_ENV !== 'development' && 'babel-loader',
    {
      loader: 'ts-loader',
      options: {
        compilerOptions: {
          declaration: true,
          experimentalDecorators: true,
          jsx: 'react',
          outDir: './dist',
          lib: ['dom', 'es2017'],
          module: 'es6',
          moduleResolution: 'node',
          sourceMap: true,
          target: 'es5',
        },
        transpileOnly: NODE_ENV === 'development',
        experimentalWatchApi: NODE_ENV === 'development',
      },
    },
  ].filter((v) => v),
  include: [path.resolve(root, 'src'), path.resolve(__dirname, 'assets.d.ts'), ...include],
  exclude: /node_modules/,
})

module.exports.jsx = ({ NODE_ENV, root, merge: { use = [], include = [] } = {} } = {}) => ({
  test: /\.jsx?$/,
  use: ['babel-loader', ...use],
  include: [path.resolve(root, 'src'), ...include],
  exclude: /node_modules/,
})

module.exports.fonts = ({ NODE_ENV, root } = {}) => ({
  test: /\.(woff|woff2|eot|ttf|otf)$/,
  use: ['file-loader'],
})
