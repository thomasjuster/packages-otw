'use strict'

function safeParse (value) {
  try {
    return JSON.parse(value)
  } catch (e) {
    return value
  }
}

module.exports = (argsToParse) => {
  const args = {}
  argsToParse.forEach((arg, index) => {
    const nextArg = argsToParse[index + 1]
    if (arg.startsWith('--')) {
      const key = arg.replace('--', '')
      let value = nextArg && !nextArg.startsWith('--') ? safeParse(nextArg) : true
      if (typeof value === 'string' && value.startsWith('[')) {
        value = value.slice(1, -1).split(',')
      }
      // eslint-disable-next-line security/detect-object-injection
      args[key] = value
    }
  })
  return args
}
