/* eslint-disable security/detect-child-process,no-console */
'use strict'

const { spawn, spawnSync } = require('child_process')

module.exports = (command, { sync = false, cwd = process.cwd(), env = process.env, ...options } = {}) => {
  options = { shell: true, stdio: 'inherit', ...options }
  const [cmd, ...args] = command.split(' ')
  if (sync) {
    return spawnSync(cmd, args, { cwd, env, ...options })
  }
  const child = spawn(cmd, args, { cwd, env, ...options })
  process.on('exit', child.kill)
  return child
}
