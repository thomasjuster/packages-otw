/* eslint-disable security/detect-child-process,security/detect-non-literal-require,security/detect-non-literal-fs-filename,no-console */
'use strict'

const path = require('path')
const webpack = require('webpack')
const spawn = require('./utils/spawn')
const libConfig = require('../webpack/typescript-lib')

const root = process.cwd()
const { NODE_ENV = 'development' } = process.env
const { name: pkgName } = require(`${root}/package.json`)

function getConfig (cliArgs) {
  const { entry, target, externals } = cliArgs
  return libConfig({
    NODE_ENV,
    root,
    entry: path.resolve(root, entry || './src/index.ts'),
    pkgName,
    output: path.resolve(root, 'dist'),
    target,
    externals,
  })
}

exports.watch = (cliArgs) => webpack(getConfig(cliArgs))
  .watch({}, (err, stats) => {
    if (err) return console.error(err)
    console.info(stats.toString({ chunks: false, colors: true }))
  })

exports.build = (cliArgs) => {
  webpack(getConfig(cliArgs), (err, stats) => {
    if (err) return console.error(err)
    console.info(stats.toString({ chunks: false, colors: true }))
  })
  spawn('rm -rf ./es/*', { sync: true })
  spawn('yarn tsc --outDir ./es --target es5 --declaration false --skipLibCheck true', { sync: true })
}
