/* eslint-disable no-console,security/detect-non-literal-fs-filename,security/detect-child-process */
'use strict'

const path = require('path')
const webpack = require('webpack')
const WebpackDevServer = require('webpack-dev-server/lib/Server')
const spawn = require('./utils/spawn')
const { client: clientConfig, server: serverConfig } = require('../webpack/typescript-react')

const { NODE_ENV = 'development' } = process.env

function getClientConfig (args) {
  return clientConfig({
    NODE_ENV,
    root: process.cwd(),
    entry: ['babel-polyfill', './src/client.tsx'],
    outfilename: NODE_ENV === 'development' ? '[name].js' : '[name]-[contenthash].js',
    publicPath: process.env.PUBLIC_PATH || '/',
    devPort: args.port || process.env.PORT || 8080,
  })
}

function getServerConfig (args) {
  return serverConfig({
    NODE_ENV,
    root: process.cwd(),
    entry: './src/server.tsx',
    filename: 'server.js',
    publicPath: process.env.PUBLIC_PATH || '/',
  })
}

exports.start = (cliArgs) => {
  const port = cliArgs.port || process.env.PORT
  const config = getClientConfig(cliArgs)
  config.entry.unshift(`webpack-dev-server/client?http://localhost:${port}/`, 'webpack/hot/dev-server')
  const compiler = webpack(config)
  const server = new WebpackDevServer(compiler, {
    compress: true,
    port,
    contentBase: path.resolve(process.cwd(), 'dist'),
    historyApiFallback: true,
    disableHostCheck: true,
    hot: true,
    inline: true,
    stats: { colors: true },
  })
  server.listen(port, 'localhost', () => console.info('Listening on', port))
}

exports.watch = (cliArgs) => {
  const watchProcess = webpack([
    { ...getClientConfig(cliArgs), watch: true },
    { ...getServerConfig(cliArgs), watch: true },
  ]).watch({}, (err, stats) => {
    if (err) return console.error(err)
    console.info(stats.toString({ chunks: false, colors: true }))
  })
  const selfRoot = path.resolve(__dirname, '..')
  const serverPath = path.relative(selfRoot, path.resolve(process.cwd(), './dist/server.js'))
  spawn(`nodemon ${serverPath}`, { cwd: selfRoot })
  process.on('exit', watchProcess.close)
}

exports.build = (cliArgs) => {
  webpack([
    getClientConfig(cliArgs),
    getServerConfig(cliArgs),
  ], (err, stats) => {
    if (err) return console.error(err)
    console.info(stats.toString({ chunks: false, colors: true }))
  })
}
