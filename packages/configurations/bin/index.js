#!/usr/bin/env node

/* eslint-disable security/detect-non-literal-fs-filename,no-console */

'use strict'

const appScripts = require('./app')
const libScripts = require('./lib')
const testScripts = require('./test')
const parseArgs = require('./utils/parse-args')

const [, , command, ...argsToParse] = process.argv
const args = parseArgs(argsToParse)

const helpMessage = `
Usage: @otw/conf <command> <OPTIONS>
- commands:
  > app:start
  > app:watch
  > app:build
  > lib:watch
  > lib:build
  > test:ts-react
  > test:ts-node
  > help

- options:
  > --port (app:* only)
  > --entry (lib:* only)
`

;(() => {
  switch (command) {
    case 'app:start': return appScripts.start(args)
    case 'app:watch': return appScripts.watch(args)
    case 'app:build': return appScripts.build(args)
    case 'lib:watch': return libScripts.watch(args)
    case 'lib:build': return libScripts.build(args)
    case 'test:ts-react': return testScripts.react(args)
    case 'test:ts-node': return testScripts.node(args)
    case 'help': return console.info(helpMessage)
    default: return console.info(`Unknown command "${command}"\n\n${helpMessage}`)
  }
})()
