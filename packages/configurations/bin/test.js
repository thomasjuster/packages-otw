'use strict'

const fs = require('fs')
const path = require('path')
const spawn = require('./utils/spawn')

const tsJestNodeConfiguration = require('../ts-jest/node')
const tsJestReactConfiguration = require('../ts-jest/react')

process.env.NODE_ENV = 'test'
process.env.BABEL_ENV = 'test'
process.on('unhandledRejection', err => { throw err })

const root = process.cwd()
const CONF_FILE_PATH = path.resolve(root, 'jest.config.js')
const FILE_MODULE_NAME_MAPPER = `${root}/moduleNameMapper.js`

const argv = process.argv.slice(3)

function getConfiguration (type) {
  switch (type) {
    case 'node': return tsJestNodeConfiguration()
    case 'react':
    default:
      return tsJestReactConfiguration({ moduleNameMapper: '<rootDir>/moduleNameMapper.js' })
  }
}
function runTestsWithConf (type) {
  const jsConf = `'use strict'; module.exports = ${JSON.stringify(getConfiguration(type))};`
  fs.writeFileSync(CONF_FILE_PATH, jsConf)
  if (type === 'react') fs.writeFileSync(FILE_MODULE_NAME_MAPPER, "module.exports = 'test-file-stub'")
  spawn(`jest --config ./jest.config.js ${argv.join()}`, { sync: true })
  if (type === 'react') fs.unlinkSync(FILE_MODULE_NAME_MAPPER)
  fs.unlinkSync(CONF_FILE_PATH)
}

exports.react = () => runTestsWithConf('react')
exports.node = () => runTestsWithConf('node')
