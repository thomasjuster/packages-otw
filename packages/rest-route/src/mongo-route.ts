import { createRestRoute, CreateRestRouteParams } from './rest-route'

export type MongoRouteParams =
  Pick<CreateRestRouteParams, 'basePath' | 'plural' | 'before' | 'router'> & {
    model: any;
    isUserRelated: boolean;
    renderListAsArray?: boolean;
  }

type Document = any;
const documentToJSON = (document: Document) => document && document.toJSON ? document.toJSON() : document
function renderDocuments (docs: Document[] = [], renderListAsArray?: boolean): Document {
  return renderListAsArray
    ? docs.map((document) => documentToJSON(document))
    : docs.reduce((acc, document: Document) => ({ ...acc, [document._id]: documentToJSON(document) }), {})
}

export function createMongoRestRoute ({ model, isUserRelated, renderListAsArray, ...params }: MongoRouteParams) {
  function findById (id: string, req?: any) {
    return model.findOne({ _id: id, userId: isUserRelated ? req.auth.user._id : undefined })
  }
  return createRestRoute({
    ...params,
    async getMany (req, res, next) {
      const docs = await model.find(isUserRelated ? { userId: req.auth.user._id } : undefined)
      res.status(200).json(renderDocuments(docs, renderListAsArray))
    },

    async getOne (req, res, next, id) {
      const document = await findById(id as string, req)
      res.status(200).json(documentToJSON(document))
    },

    async find (req, res, next) {
      const conditions = {
        userId: isUserRelated ? req.auth.user._id : undefined,
        ...Object
          .keys(req.query.search || {})
          .reduce((acc, key) => ({ ...acc, [key]: new RegExp(req.query.search[key], 'gi') }), {}),
      }
      const size = parseInt(req.query.size, 10) || 20
      const offset = parseInt(req.query.offset, 10) || 0
      const options = {
        limit: size,
        skip: offset,
        sort: req.query.sort || { _id: 'asc' }, // sort already has shape { [field]: 'asc' | 'desc' }
      }
      const items = model.find(conditions, null, options)
      const count = model.countDocuments(conditions)
      res.status(200).json({ results: await items, total: await count, size, offset })
    },

    async createMany (req, res, next) {
      const body = {
        ...req.body,
        userId: isUserRelated ? req.auth.user._id : undefined,
      }
      const created = await model.create(body)
      res.status(201).json(documentToJSON(created))
    },

    async replaceOne (req, res, next, id) {
      const doc = await model.findOneAndReplace({ _id: id, userId: isUserRelated ? req.auth.user._id : undefined }, undefined, req.body)
      if (!doc) return res.sendStatus(404)
      const updatedDocument = await model.findById(doc._id)
      res.status(200).json(documentToJSON(updatedDocument))
    },

    async updatePartiallyOne (req, res, next, id) {
      const doc = await model.findOneAndUpdate({ _id: id, userId: isUserRelated ? req.auth.user._id : undefined }, req.body)
      if (!doc) return res.sendStatus(404)
      const updatedItem = await model.findById(doc._id)
      res.status(200).json(documentToJSON(updatedItem))
    },

    async deleteOne (req, res, next, id) {
      await model.deleteOne({ _id: id, userId: isUserRelated ? req.auth.user._id : undefined })
      res.sendStatus(204)
    },
  })
}
