import express from 'express'
import supertest from 'supertest'
import { createRestRoute, CreateRestRouteParams, Method, RouteResponse } from './rest-route'

// tslint:disable-next-line:interface-name
interface Tag { id: string, name: string }
const tagsFactory = (count = 1): Tag[] => new Array(count).fill(null).map((meh, i) => ({
  id: `id ${i}`,
  name: `name ${i}`,
}))

function requestApp (methods = {}) {
  const app = express()
  app.use(createRestRoute({
    basePath: '/v1',
    plural: 'tags',
    ...methods,
  }))

  app.use((error: any, req: any, res: any, next: any) => {
    res.status(500).json({ error })
  })

  return supertest(app)
}

describe('createRestRoute', () => {
  let tags: Tag[]
  let getOne: any
  beforeEach(() => {
    tags = tagsFactory(3)
    getOne = jest.fn().mockResolvedValue({ status: 200, data: tags[0] })
  })

  it('should fail with bad config', () => {
    expect(() => requestApp({ plural: null }))
      .toThrow('Could not create rest route because:\n  × Missing parameter "plural"\n  × You must declare at least one of the following methods: "getMany", "getOne", "find", "createMany", "replaceOne", "updatePartiallyOne", "deleteOne"')
  })

  describe('"before" middlewares', () => {
    let find: any;
    let middleware: any
    beforeEach(() => {
      find = jest.fn().mockResolvedValue({ status: 200, data: { tags: [tags[0], tags[2]] } })
      middleware = jest.fn((req, res, next) => {
        req.TEST_VALUE = 'TOTO'
        next()
      })
    })

    it('should apply without path', async () => {
      const app = requestApp({ getOne, find, before: [{ middleware }] })
      await Promise.all([
        app.get('/v1/tags/search').expect(200),
        app.get('/v1/tags/1').expect(200)
      ])
      expect(middleware).toHaveBeenCalledTimes(2)
      expect(find.mock.calls[0][0].TEST_VALUE).toBe('TOTO')
      expect(getOne.mock.calls[0][0].TEST_VALUE).toBe('TOTO')
    })

    it('should apply with path "*"', async () => {
      const app = requestApp({ getOne, find, before: [{ path: '*', middleware }] })
      await Promise.all([
        app.get('/v1/tags/search').expect(200),
        app.get('/v1/tags/1').expect(200)
      ])
      expect(middleware).toHaveBeenCalledTimes(2)
      expect(find.mock.calls[0][0].TEST_VALUE).toBe('TOTO')
      expect(getOne.mock.calls[0][0].TEST_VALUE).toBe('TOTO')
    })

    it('should apply with path', async () => {
      const app = requestApp({ getOne, find, before: [{ path: '/search', middleware }] })
      await Promise.all([
        app.get('/v1/tags/search').expect(200),
        app.get('/v1/tags/1').expect(200)
      ])
      expect(middleware).toHaveBeenCalledTimes(1)
      expect(find.mock.calls[0][0].TEST_VALUE).toBe('TOTO')
      expect(getOne.mock.calls[0][0].TEST_VALUE).toBeUndefined()
    })
  })

  describe('GET /resources', () => {
    it('should send 404', async () => {
      return requestApp({ getOne }).get('/v1/tags').expect(404)
    })

    it('should prevent sending response', async () => {
      const getMany = jest.fn(async (request, response) => {
        response.status(203).json({ orange: 'is the new black' })
      })
      const res = await requestApp({ getOne, getMany })
        .get('/v1/tags')
        .expect(203)
      expect(res.body).toEqual({ orange: 'is the new black' })
    })

    it('should send successful response', async () => {
      const getMany = jest.fn().mockResolvedValue({ status: 200, data: { tags } })
      const res = await requestApp({ getOne, getMany })
        .get('/v1/tags')
        .expect(200)
      expect(res.body).toEqual({ tags })
    })

    it('should send error response', async () => {
      const getMany = jest.fn().mockRejectedValue(new Error('Some Error'))
      await requestApp({ getOne, getMany })
        .get('/v1/tags')
        .expect(500)
    })
  })

  describe('GET /resources/search', () => {
    it('should send 404', async () => {
      return requestApp({ getOne }).get('/v1/tags/search').expect(404)
    })

    it('should send successful response', async () => {
      const find = jest.fn().mockResolvedValue({ status: 200, data: { tags: [tags[0], tags[2]] } })
      const res = await requestApp({ getOne, find })
        .get('/v1/tags/search')
        .expect(200)
      expect(res.body).toEqual({ tags: [tags[0], tags[2]] })
    })

    it('should send error response', async () => {
      const find = jest.fn().mockRejectedValue(new Error('Some Error'))
      await requestApp({ getOne, find })
        .get('/v1/tags/search')
        .expect(500)
    })
  })

  describe('POST /resources', () => {
    it('should send 404', async () => {
      return requestApp({ getOne }).post('/v1/tags').expect(404)
    })

    it('should send successful response', async () => {
      const createMany = jest.fn().mockResolvedValue({ status: 201, data: { tags: [tags[0], tags[2]] } })
      const res = await requestApp({ getOne, createMany })
        .post('/v1/tags')
        .expect(201)
      expect(res.body).toEqual({ tags: [tags[0], tags[2]] })
    })

    it('should send error response', async () => {
      const createMany = jest.fn().mockRejectedValue(new Error('Some Error'))
      await requestApp({ getOne, createMany })
        .post('/v1/tags')
        .expect(500)
    })
  })

  describe('GET /resources/<id>', () => {
    it('should send successful response', async () => {
      getOne = jest.fn().mockResolvedValue({ status: 200, data: { tag: tags[1] } })
      const res = await requestApp({ getOne })
        .get('/v1/tags/1')
        .expect(200)
      expect(res.body).toEqual({ tag: tags[1] })
    })

    it('should send error response', async () => {
      getOne = jest.fn().mockRejectedValue(new Error('Some Error'))
      return requestApp({ getOne })
        .get('/v1/tags/1')
        .expect(500)
    })
  })

  describe('PUT /resources/<id>', () => {
    it('should send 404', async () => {
      return requestApp({ getOne }).put('/v1/tags/2').expect(404)
    })

    it('should send successful response', async () => {
      const replaceOne = jest.fn().mockResolvedValue({ status: 200, data: { tag: tags[2] } })
      const res = await requestApp({ getOne, replaceOne })
        .put('/v1/tags/2')
        .expect(200)
      expect(res.body).toEqual({ tag: tags[2] })
    })

    it('should send error response', async () => {
      const replaceOne = jest.fn().mockRejectedValue(new Error('Some Error'))
      await requestApp({ getOne, replaceOne })
        .put('/v1/tags/2')
        .expect(500)
    })
  })

  describe('PATCH /resources/<id>', () => {
    it('should send 404', async () => {
      return requestApp({ getOne }).patch('/v1/tags/2').expect(404)
    })

    it('should send successful response', async () => {
      const updatePartiallyOne = jest.fn().mockResolvedValue({ status: 200, data: { tag: tags[2] } })
      const res = await requestApp({ getOne, updatePartiallyOne })
        .patch('/v1/tags/2')
        .expect(200)
      expect(res.body).toEqual({ tag: tags[2] })
    })

    it('should send error response', async () => {
      const updatePartiallyOne = jest.fn().mockRejectedValue(new Error('Some Error'))
      await requestApp({ getOne, updatePartiallyOne })
        .patch('/v1/tags/2')
        .expect(500)
    })
  })

  describe('DELETE /resources/<id>', () => {
    it('should send 404', async () => {
      return requestApp({ getOne }).delete('/v1/tags/2').expect(404)
    })

    it('should send successful response with object', async () => {
      const deleteOne = jest.fn().mockResolvedValue({ status: 200, data: { tag: tags[2] } })
      const res = await requestApp({ getOne, deleteOne })
        .delete('/v1/tags/2')
        .expect(200)
      expect(res.body).toEqual({ tag: tags[2] })
    })

    it('should send successful response without object', async () => {
      const deleteOne = jest.fn().mockResolvedValue({ status: 204, data: undefined })
      await requestApp({ getOne, deleteOne })
        .delete('/v1/tags/2')
        .expect(204)
    })

    it('should send error response', async () => {
      const deleteOne = jest.fn().mockRejectedValue(new Error('Some Error'))
      await requestApp({ getOne, deleteOne })
        .delete('/v1/tags/2')
        .expect(500)
    })
  })
})
