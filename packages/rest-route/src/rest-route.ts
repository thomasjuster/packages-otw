import * as express from 'express'

export type Middleware = (request: any, response: any, next: any, param?: string) => void | Promise<void>;

// tslint:disable-next-line:interface-name
export type RouteResponse<T> = Promise<{
  status?: number;
  data: T;
} | void>
// tslint:disable-next-line:interface-name
export type Method = (request: any, response: any, next: any, id?: string) => RouteResponse<any>;

// tslint:disable-next-line:interface-name
function useProvidedMethod (method?: Method): Middleware {
  return async (request, response, next) => {
    if (!method) return response.sendStatus(404)
    try {
      // these two steps are required to effectively catch an error
      const result = await method(request, response, next, request.resourceId)
      if (!result) return
      return response.status(result.status || 200).json(result.data || {})
    } catch (err) {
      next(err)
    }
  }
}

// tslint:disable-next-line:interface-name
export interface CreateRestRouteParams {
  basePath?: string;
  plural: string;
  before?: Array<{ path?: string, middleware: (request: any, response: any, next: any) => void }>;
  router?: express.Router;
  getMany?: Method; // GET /resources
  getOne?: Method; // GET /resources/<id>
  find?: Method; // GET /resources/search?...
  createMany?: Method; // POST /resources
  replaceOne?: Method; // PUT /resources/<id>
  updatePartiallyOne?: Method; // PATCH /resources/<id>
  deleteOne?: Method; // DELETE /resources/<id>
}

export function createRestRoute ({
  basePath = '',
  plural,
  before = [],
  router = express.Router(),
  getMany,
  getOne,
  find,
  createMany,
  replaceOne,
  updatePartiallyOne,
  deleteOne,
}: CreateRestRouteParams) {
  const noMethodsDeclared = !getMany && !getOne && !find && !createMany && !replaceOne && !updatePartiallyOne && !deleteOne
  const errors: string[] = []
  if (!plural) errors.push('  × Missing parameter "plural"')
  if (noMethodsDeclared) errors.push('  × You must declare at least one of the following methods: "getMany", "getOne", "find", "createMany", "replaceOne", "updatePartiallyOne", "deleteOne"')
  if (errors.length) {
    throw new Error(`Could not create rest route because:\n${errors.join('\n')}`)
  }

  const BASE = `${basePath}/${plural}`
  const PARAM_KEY = 'id'

  before.forEach(({ path, middleware }) => {
    path ? router.use(`${BASE}${path}`, middleware) : router.use(middleware)
  })

  router.param(PARAM_KEY, (async (request, response, next, id) => {
    request.resourceId = id
    next()
  }) as Middleware)

  // ROUTE /resources
  router
    .route(BASE)
    .get(useProvidedMethod(getMany))
    .post(useProvidedMethod(createMany))

  // ROUTE /resources/search
  router
    .route(`${BASE}/search`)
    .get(useProvidedMethod(find))

  // GET /resources/<id>
  router
    .route(`${BASE}/:${PARAM_KEY}`)
    .get(useProvidedMethod(getOne))
    .put(useProvidedMethod(replaceOne))
    .patch(useProvidedMethod(updatePartiallyOne))
    .delete(useProvidedMethod(deleteOne))

  return router
}
