## Commands

Test all projects/packages `lerna run test`

Launch all projects/packages in development mode `lerna run start`

Build all projects/packages `lerna run build`

<!-- Deploying `lerna run deploy` (WIP) -->

<!-- ## Roadmap -->